import React from 'react'
import Logo from '../../component/logo/logo'
import {Radio, InputItem, WingBlank, WhiteSpace, Button} from 'antd-mobile'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {register} from '../../redux/user.redux'
import imoocForm from '../../component/imooc-form/imooc-form';

@connect(
    //function mapStateToProps(state)( return state.user)
    state=>state.user,
    {register}
)
@imoocForm
class Register extends React.Component{
    constructor(props){
        super(props);
        this.handleRegister = this.handleRegister.bind(this)
    }
    componentDidMount(){
        this.props.handleChange('type','genius')
    }
    handleRegister(){
        this.props.register(this.props.state);
    }
    render(){
        const RadioItem = Radio.RadioItem
        return (
            <div>
                {/* 返回这个参数后导向别的页面 */}
                {this.props.redirectTo?<Redirect to={this.props.redirectTo}/>:null}
               <Logo/>
                <WingBlank>
                    {this.props.msg?<p className='error-msg'>{this.props.msg}</p>:null}
                    <InputItem 
                      onChange={v=>this.props.handleChange('user',v)}
                    placeholder="账号"></InputItem>
                    <WhiteSpace/> 
                    <InputItem
                       onChange={v=>this.props.handleChange('pwd',v)}
                       type={"password"}
                    placeholder="密码"></InputItem>
                    <WhiteSpace/> 
                    <InputItem 
                       onChange={v=>this.props.handleChange('repeatpwd',v)}
                       type={"password"}
                       placeholder="确认密码"></InputItem>
                    <RadioItem 
                       checked={this.props.state.type==='genius'}
                       onChange={()=>this.props.handleChange('type','genius')}    
                    >
                        牛人
                    </RadioItem>
                    <RadioItem 
                       checked={this.props.state.type==='boss'}
                       onChange={()=>this.props.handleChange('type','boss')}    
                    >
                        猎人
                    </RadioItem>
                    <Button type='primary' onClick={this.handleRegister}>注册</Button>   
                </WingBlank>    
            </div>
           ) 
    }
}
export default Register