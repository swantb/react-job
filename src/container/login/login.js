import React from 'react'
import Logo from '../../component/logo/logo'
import {InputItem, WingBlank, WhiteSpace, Button} from 'antd-mobile'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {login} from '../../redux/user.redux'
import imoocForm from './../../component/imooc-form/imooc-form';
@connect(
    state=>state.user,
    {login}
)
@imoocForm
class Login extends React.Component{
    constructor(props){
        super(props);
        this.handleLogin = this.handleLogin.bind(this)
    }
    register() {
        this.props.history.push('./register')
    }
    handleLogin() {
        this.props.login(this.props.state)
    }
    render(){
        return (
            <div>
                   {/* 返回这个参数后导向别的页面 */}
                   {this.props.redirectTo?<Redirect to={this.props.redirectTo}/>:null}
               <Logo/>
                <WingBlank>
                    {this.props.msg?<p className='error-msg'>{this.props.msg}</p>:null}
                    <InputItem placeholder="账号"
                      onChange={v=>this.props.handleChange('user',v)}
                    ></InputItem>
                    <WhiteSpace/> 
                    <InputItem placeholder="密码" type="password"
                      onChange={v=>this.props.handleChange('pwd',v)}
                    ></InputItem>
                    <WhiteSpace/> 
                    <Button type='primary' onClick={this.handleLogin}>登录</Button> 
                    <WhiteSpace/>  
                    <Button onClick={()=>this.register()} type='primary'>注册</Button>   
                </WingBlank>    
            </div>
           ) 
    }
}
export default Login