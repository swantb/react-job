import React from 'react';
import ReactDom from 'react-dom'
import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import { Provider} from 'react-redux'
import {Switch, BrowserRouter, Route} from 'react-router-dom'

import Login from './container/login/login'
import Register from './container/register/register'
import AuthRoute from './component/authroute/authroute';
import reducers from './reducer'
import GeniusInfo from './container/geniusinfo/geniusinfo'
import BossInfo from './container/bossinfo/bossinfo'
import Chat from './component/chat/chat'
import Dashboard from './component/dashboard/dashboard'
import './config'
import './index.css'


const store = createStore(reducers, compose(
    applyMiddleware(thunk),
    window.devToolsExtension?window.devToolsExtension():f=>f
))
// boss genius me msg 4个页面
ReactDom.render(
    (<Provider store={store}> 
      <BrowserRouter>
       <div>
          <AuthRoute></AuthRoute>
          <Switch>
            <Route path='/geniusinfo' component={GeniusInfo}></Route>  
            <Route path='/bossinfo' component={BossInfo}></Route>  
            <Route path='/Login' component={Login}>登陆</Route>
            <Route path='/Register' component={Register}>注册</Route>
            <Route path='/chat/:user' component={Chat}>聊天</Route>          
            <Route  component={Dashboard}>注册</Route>
          </Switch>        
       </div>
      </BrowserRouter>
    </Provider>),
  document.getElementById('root')
)

