import React from 'react'

export default function imoocForm(Comp){
    return class WrapperComp extends React.Component{
        constructor(props){
            super(props)
            this.state = {}
            this.handleChange =this.handleChange.bind(this)
        }
        handleChange(key,val){
            this.setState({
                [key]:val
            })
        }
        render(){
            //传props进去的目的就是让上面的高阶connect起作用
            return <Comp handleChange={this.handleChange} state={this.state} {...this.props}></Comp>
        }
    }
}