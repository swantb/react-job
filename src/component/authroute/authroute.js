import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {loadData} from '../../redux/user.redux'
//让子组件拥有动态的路由
@withRouter
@connect(
  null, {loadData}
)
class AuthRoute extends React.Component{
    componentDidMount(){
        const publicList = ['/login','/register']
        const pathname = this.props.location.pathname
        if (publicList.indexOf(pathname)>-1) {
            return null
        }
        //获取用户信息是否登录，现在的url地址，login不需要跳转，用户的type身份是boss还是牛人
       axios.get('/user/info').
         then(res=>{
             if (res.status===200){
                 if(res.data.code===0){
                     //有登陆信息的
                     this.props.loadData(res.data.data)
                 }else{
                     this.props.history.push('/login');
                  }
             }
         })
    }
    render() {
        return null
    }
}
export default AuthRoute