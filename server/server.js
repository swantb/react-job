const model = require('./model')
const Chat = model.getModel('chat')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const app = express()
// work with express
const server = require('http').Server(app)

const io = require('socket.io')(server)
//socketio
io.on('connection',function(socket){
    console.log('user login')
    socket.on('sendmsg',function(data){
        const {from,to,msg} =data
        console.log(data)
        const chatid =[from,to].sort().join('_')
        Chat.create({chatid,from,to,content:msg},function(err,doc){
                io.emit('recvmsg',Object.assign({},doc._doc))
        })
    })
})
const userRouter = require('./user')

// 解析cookie
app.use(cookieParser())
//解析post过来的json
app.use(bodyParser.json())
//导入子路由
app.use('/user',userRouter)

server.listen(9093,function(){
    console.log('node app start at port 9093')
})