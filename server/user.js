const express = require('express')
const utility = require('utility')
const Router = express.Router()
const model = require('./model')
const User = model.getModel('user') 
const Chat = model.getModel('chat')
//定义一个查询条件，不要显示pwd
const _filter = {'pwd':0,'_v':0}
Router.get('/list',function(req,res){
   // User.remove({},function(err,doc){
   //    return res.json(doc)
   // })
   const {type} = req.query
   User.find({type},function(err,doc){
      return res.json({code:0,data:doc})
   })
})
Router.post('/login', function(req,res){
   const {user, pwd} =req.body
   User.findOne({user,pwd:newMd5(pwd)},_filter,function(err,doc){
      if(!doc){
         return res.json({code:1,msg:'用户名密码输入错误'})
      }
      else{
         //保存mongodb生成的唯一标识,这个函数来源于cookie-parser
         res.cookie('userid',doc._id)
          return res.json({code:0,data:doc})
      }
   })
})
Router.get('/getmsgList',function(req,res){
   const user= req.cookies.userid
   User.find({},function(e,userdoc){
      let users = {}
      userdoc.forEach(v=>{
         users[v._id] = {name:v.user,avatar:v.avatar}
      })
      Chat.find({'$or':[{from:user},{to:user}]},function(err,doc){
         if(!err) {
            return res.json({code:0,msgs:doc,users:users})
         }
      })
   })
})
Router.post('/register',function(req,res){
   const {user, pwd, type} =req.body
   User.findOne({user},function(err,doc){
      if(doc){
         return res.json({code:1,msg:'用户名重复,请注册新的账号'})
      }
      const userModel = new User({user,type,pwd:newMd5(pwd)})
      userModel.save(function(e,d){
         if(e){
            return res.json({code:1,msg:'后端错误'})
         }
         const {user, type, _id}=d
         res.cookie('userid',_id)
         return res.json({code:0,data:{user,type,_id}})
      })
   })
})
Router.get('/info',function(req,res){
   // 读cookie要从req
   const {userid} =req.cookies
   if(!userid) {
      return res.json({code:1})
   }else{
       User.findOne({_id:userid},_filter,function(err,doc){
      if(doc){
          return res.json({code:0,data:doc})
      }
   })
   }
})
//消息已读
Router.post('/readmsg',function(req,res){
   const userid = req.cookies.userid
   const {from} = req.body
   console.log(userid,from)
   //更改他发给我的消息的unread
   Chat.update(
      {from,to:userid},
      {'$set':{read:true}},
      {'multi':true},
      function(err,doc){
         console.log(doc)
      if(!err){
         return res.json({code:0,num:doc.nModified})
      }
      return res.json({code:1,msg:'修改失败'})
   })
})
//完善注册信息
Router.post('/update',function(req,res){
   const userid = req.cookies.userid
   if(!userid){
      return res.json.dumps({code:1})
   }
   const body = req.body
   User.findByIdAndUpdate(userid,body,function(err,doc){
      const data =Object.assign({},{
         user:doc.user,
         type:doc.type
      },body)
      return res.json({code:0,data})
   })
})
function newMd5(pwd){
   const salt = 'ilu#$ilu@qunar'
   return utility.md5(utility.md5(pwd+salt))
}
module.exports = Router