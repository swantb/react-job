const mongoose = require('mongoose')
//连接mongo 并且使用imooc这个集合
const DB_URL = 'mongodb://localhost:27017/imooc-chat'
mongoose.connect(DB_URL)

// schema是mongoose里会用到的一种数据模式，可以理解为表结构的定义；
const models = {
    user:{
        'user':{'type':String, 'require':true},
        'pwd':{'type':String,'require':true},
        'type':{'type':String,'require':true},
        //头像
        'avatar':{'type':String},
        //个人简介
        'desc':{'type':String},
        //职位名
        'title':{'type':String},
        //如果你是boss，还有两个字段
        'company':{'type':String},
        'money':{'type':String}
    },
    chat:{
        'chatid':{'type':String,require:true},
        'from':{'type':String,require:true},
        'to':{'type':String,require:true},
        'read':{'type':Boolean,require:false},
        'content':{'type':String,require:true,default:''},
        'create_time':{'type':Number,default:new Date().getTime()}
    }
}
//生成schema和model
for (let m in models){
    mongoose.model(m, new mongoose.Schema(models[m]))
}
//到处模块的引用函数
module.exports = {
    getModel:function(name){
        return mongoose.model(name)
    }
}